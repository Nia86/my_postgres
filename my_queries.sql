select * from my_table


-- select film_id, title,replacement_cost from film where title = 'Arizona Bang';
-- select * from payment where amount > 11 and staff_id = 1
-- select count(distinct district) from address;
-- select * from customer order by customer_id;
-- select film_id, title, rental_rate, replacement_cost from film where rental_rate = '2.99' and replacement_cost ='26.99' order by title desc;
-- select film_id, title, rating, replacement_cost from film where film_id in (2,12,23,32,50) order by title;
-- select film_id, title, rating, rental_rate from film where rating not in ('R','G','PG') order by title;
-- select count(*) from payment where staff_id = 1 and payment_date between '2007-02-01' and '2007-02-28';
-- select first_name, last_name, email from customer where first_name ='Kelly' and last_name like 'K%';
-- select count(distinct rating) from film;
-- select distinct rating from film;
-- select distinct amount from payment order by amount desc limit 5;
-- select staff_id, sum(amount) from payment group by staff_id;
-- select rating, round(AVG(replacement_cost),2) from film group by rating;
-- select rental_rate, count(rental_rate) from film group by rental_rate;
-- select rating, round(AVG(rental_rate),2) from film group by rating having round(AVG(rental_rate),2) >3;
-- select customer_id,staff_id, count(amount) from payment group by customer_id,staff_id having staff_id='1' order by count(amount) desc;
-- select staff_id, count(amount)from payment where payment_date between '2007-03-01' and '2007-03-31' group by staff_id;
-- select count(film_id) from film where length > 120 and rating = 'R';
-- select title from film where title like 'K%' order by replacement_cost desc limit 1;
-- select rating, count(film_id) from film where replacement_cost in (10.99,11.99) group by rating having count(film_id) >20;
-- select category_id from film inner join film_category on film.film_id =film_category.film_id;
select film_id,rental_duration from film where rental_duration >(select round(AVG(rental_duration),2) from film);
