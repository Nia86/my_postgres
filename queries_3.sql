-- 1. look all info from table members
-- SELECT * FROM cd.members;
-- 2.list of phone numbers of all memmbers of the club. print first name, last name, phone number.
-- select firstname,surname,telephone from cd.members order by surname
-- 3.print list of members of the club, who live in Boston.
-- select surname, firstname, address from cd.members where address like 'Boston';
-- 4.print list of facilities which cost for club members is less than 1/50 of service cost per month
-- select facid, membercost from cd.facilities
-- where  membercost < monthlymaintenance/50;
-- 5.count quantity of equipment, which is free for club members
-- select * from cd.facilities;
-- select count (name) from cd.facilities where membercost =0;
-- 6.print list of clubmembers, who join in august 2012
-- select surname, firstname, joindate from cd.members where joindate between '2012-08-01' and '2012-08-31';
-- 7.print list of equipment with ID 1,4,5.
-- select facid, name from cd.facilities where facid in (1,4,5);
-- 8.print list of facilities which name has word 'Court'
-- select facid, name from cd.facilities where name ilike '%Court%';
-- 9.how you can make the list in order of the first 10 lastnames in the table of joiners. the list mustn't have copies.
-- select distinct surname from cd.members order by surname limit 10;
-- 10.when was join the first member of the club
-- select memid, joindate from cd.members group by memid order by joindate limit 1;
-- SELECT MIN(joindate) AS first_signup
-- FROM cd.members;
-- 11.print 5 club members(ID) who booked maximum quantity of slots. Sort list by slots (desc)
-- SELECT memid,SUM(slots)
-- FROM cd.bookings
-- GROUP BY memid
-- ORDER BY by SUM(slots) DESC
-- LIMIT 5;
-- 12.Выведите список ID оборудования, у которого сумма интервалов бронирования(slots) >1000. Отсортируйте с список в порядке возрастания по сумме интервалов бронирования.
-- SELECT facid, SUM(slots) AS total_slots
-- FROM cd.bookings
-- GROUP BY facid
-- HAVING SUM(slots) > 1000
-- ORDER BY total_slots ;
-- 13.Получите список оборудования с самой низкой стоимостью для гостей клуба.
-- select facid, name, min(guestcost) from cd.facilities group by facid;
-- SELECT name,guestcost
-- FROM cd.facilities
-- WHERE guestcost = (SELECT min(guestcost)from cd.facilities);
-- 14.Как можно получить список бронирований Массажных комнат 10.07.2012?
-- Таблица должна содержать колонки starttime (дата и время бронирования) и name (название услуги).
-- select * from cd.facilities;
-- select name, starttime from cd.facilities inner join cd.bookings on cd.facilities.facid = cd.bookings.facid;
-- FROM cd.facilities facs
-- INNER JOIN cd.bookings bks ON facs.facid = bks.facid;
-- WHERE facs.facid IN (4,5) and bks.starttime >= '2012-07-10' AND bks.starttime < '2012-07-11'
-- ORDER BY bks.starttime;
-- 15.Нужно узнать, какое оборудование использовал член клуба Douglas Jones.
select distinct name from cd.facilities
inner join cd.bookings on cd.facilities.facid=cd.bookings.facid
inner join cd.members
on cd.bookings.memid= cd.members.memid
where surname ='Jones'
and firstname ='Douglas';
